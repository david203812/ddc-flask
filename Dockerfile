FROM python:3.8

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD ./requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app
EXPOSE 80

CMD gunicorn --workers 2 --bind 0.0.0.0:80 app:app