from flask import jsonify, Response


def Success(response):
    return jsonify(response), 200


def Created(response):
    response["status"] = "success"
    return jsonify(response), 201


def Fail(message):
    response = {"status": "fail", "message": message}
    return jsonify(response), 400
