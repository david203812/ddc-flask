# DDC Flask service
## Getting started
First, make sure `Docker` is installed on your machine. If you have trouble with that, please refer to Docker's [official guide](https://www.docker.com/get-started)  

Then, run the following commands with your own DDC infos
```bash 
git clone git@gitlab.com:david203812/ddc-flask.git

cd ddc-flask

docker build -t ddc-flask .
docker run --name ddc-flask-dev -d\
        -e HOST=https://opbningxia.bsngate.com:18602/api/{your-project-id}/rpc\
        -e CHAIN_ID=5555\
        -e ADDRESS={your-chain-account}\
        -e PRIVATE_KEY={your-private-key}\
        -p 5555:80\
        ddc-flask 
```
You'll then be able to use DDC service on `http://localhost:5555`

## Api list
All apis the official DDC contract offers can be looked up in `ddc721_abi.json` and `ddc1155_abi.json`, you can add it to the flask file accordingly.  
In the original version of this image we've already set up some basic functions for you as follow.

### Mint
url: `/mint`  
method: `POST`  
req:  
```json
{
    "uri": "uri of DDC<str>",
    "to": "address minting to <address>"
}
```
res:
```json
{
    "receipt": "eth transaction receipt"
}
```

### Owner of a DDC
url: `/owner/<ddcid>`  
method: `GET`  
res: 
```json
{
    "owner": "DDC owner's chain account <address>"
}
```

### URI of a DDC
url: `/uri/<ddcid>`  
method: `GET`  
res:
```json
{
    "uri": "uri of the DDC <str>"
}
```

### Transfering a DDC
url: `/transfer`  
method: `POST`  
req:  
```json
{
    "from": "address transfering from <address>", 
    "to": "address transfering to <address>",
    "ddic": "id of the DDC being transfered <int>"
}
```
res:
```json
{
    "receipt": "eth transaction receipt"
}
```



