from web3 import Web3

import os
import time
import json

host = os.getenv('HOST')
chain_id = int(os.getenv('CHAIN_ID'))
address = os.getenv('ADDRESS')
private_key = os.getenv('PRIVATE_KEY')


def get_w3():
    return Web3(Web3.HTTPProvider(host))


def get_receipt(tx_hash, seconds: int = 1):
    w3 = get_w3()
    while True:
        try:
            return w3.eth.wait_for_transaction_receipt(tx_hash)
        except:
            time.sleep(seconds)


def transact(function):
    w3 = get_w3()
    nonce = w3.eth.getTransactionCount(address)
    tx = function.buildTransaction({
        "gasPrice": w3.eth.gas_price,
        "chainId": chain_id,
        "from": address,
        "nonce": nonce
    })
    signed_tx = w3.eth.account.sign_transaction(tx, private_key)

    return w3.eth.send_raw_transaction(signed_tx.rawTransaction)


def contract(abi_path: str, contract_address: str):
    w3 = get_w3()
    with open(abi_path) as f:
        abi = json.load(f)

    return w3.eth.contract(address=contract_address, abi=abi)
