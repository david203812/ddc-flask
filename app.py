from flask import Flask, request
from responses import Success
from utils import contract, transact, get_receipt

import logging
import os

app = Flask(__name__)

LOGGER = logging.getLogger("gunicorn.error")
app.logger.handlers.extend(LOGGER.handlers)
app.logger.setLevel(logging.DEBUG)

logger = app.logger


DDC721_ADDRESS = '0xad3B52B4F4bd9198DC69dD9cE4aC9846667461a2'


def get_ddc(num):
    if num == 721:
        return contract('ddc721_abi.json', DDC721_ADDRESS)
    else:
        return None


@app.route("/owner/<ddcid>", methods=["GET"])
def owner(ddcid):
    ddc_id = int(ddcid)
    ddc = get_ddc(721)

    owner = ddc.functions.ownerOf(ddc_id).call()

    return Success({"owner": owner})


@app.route("/uri/<ddcid>", methods=["GET"])
def ddc_uri(ddcid):
    ddc_id = int(ddcid)
    ddc = get_ddc(721)

    uri = ddc.functions.ddcURI(ddc_id).call()

    return Success({"uri": uri})


@app.route("/mint", methods=["POST"])
def mint():
    data = request.json
    ddc = get_ddc(721)

    tx_hash = transact(ddc.functions.mint(
        data['to'],
        data['uri']
    ))

    receipt = get_receipt(tx_hash)
    return Success({'receipt': receipt})


@app.route("/transfer", methods=["POST"])
def transfer():
    data = request.json
    ddc = get_ddc(721)

    tx_hash = transact(ddc.functions.transferFrom(
        data['from'],
        data['to'],
        data['ddcId']
    ))

    receipt = get_receipt(tx_hash)
    return Success({'receipt': receipt})
